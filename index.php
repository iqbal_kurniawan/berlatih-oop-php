<?php 
require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal("shaun");
echo $sheep->name.'<br>'; // "shaun"
echo $sheep->legs.'<br>'; // 2
echo $sheep->cold_blooded.'<br>'; // false

$sungokong = new Ape('kera sakti');
$sungokong->yell();
echo '<br>';
//$sungokong->legs;
//var_dump($sungokong);
$kodok = new Frog('buduk');
$kodok->legs = 4;
$kodok->jump();
?>